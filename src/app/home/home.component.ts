import { Component, OnInit } from '@angular/core';
import { faDrumstickBite , faIceCream} from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  faDrumstickBite = faDrumstickBite; 
  faIceCream = faIceCream;

  constructor() { }

  ngOnInit() {
  }

  openEatReact() {
    window.open(environment.urlBase + 'eat/');
  }
}
