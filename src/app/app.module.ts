import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { DesertComponent } from './desert/desert.component';
import { HomeComponent } from './home/home.component';
import { appRoutes } from './routes';

@NgModule({
   declarations: [
      AppComponent,
      DesertComponent,
      HomeComponent
   ],
   imports: [
      BrowserModule,
      FontAwesomeModule,
      RouterModule.forRoot(appRoutes)
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
