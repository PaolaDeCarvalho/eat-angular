import {Routes} from '@angular/router';
import { DesertComponent } from './desert/desert.component';
import { HomeComponent } from './home/home.component';

export const appRoutes : Routes = [
    { path: 'home' , component: HomeComponent},
    { path: 'desert', component: DesertComponent},
    { path: '**', component: HomeComponent, pathMatch: 'full'}
]