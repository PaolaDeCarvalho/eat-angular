import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-desert',
  templateUrl: './desert.component.html',
  styleUrls: ['./desert.component.scss']
})


export class DesertComponent implements OnInit {
  sobremesas = ["Merengue de morango", "Torta de Limão", "Torta de Morango", "Bolo de Chocolate", "Pudim", "Tiramissu", "Mousse de chocolate", "Pizza de Banana", "Sorvete com chantilly", "Pão com nutella", "Pão de queijo com doce de leite", "Donuts", "Bolinho de chuva", "Panqueca com frutas vermelhas", "Cocada"];

  sobremesa : any;
  constructor() { }

  ngOnInit() {

  }

  sort(){
    this.sobremesa = this.sobremesas[Math.floor(Math.random() * (this.sobremesas.length -1))];
  }

}
